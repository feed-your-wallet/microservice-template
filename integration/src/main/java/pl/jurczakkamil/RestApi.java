package pl.jurczakkamil;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Component
public class RestApi extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration()
                .contextPath("{{server.servlet.context-path}}")
                .port("{{server.port}}")
                .enableCORS(true)
                .apiContextPath("{{server.servlet.context-path}}")
                .apiProperty("api.title", "{{spring.application.name}}")
                .apiProperty("api.version", "{{application.version}}")
                .apiContextRouteId("doc-api")
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest("/api")
                .get("/hello").consumes(APPLICATION_JSON)
                .to("direct:hello");
    }
}
